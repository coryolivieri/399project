function handleSaveResult(resp_body) {
    console.log( resp_body );
    $("#feedback").text( JSON.stringify( resp_body) );
    sessionStorage.setItem('name', resp_body.user);
    if( resp_body.url ) window.location = resp_body.url; //load main app page
};

var input_main = function () {
    $("#Search").on("click", function (event) {
        window.location.replace("index.html");
    });
    $("#save").on("click", function (event) {
        $.post("saveQuestion.json",
            {"company": $("#Company").val(), "qtype": $("#QType").val(), "sub_type": $("#SubType").val(), "question": $("#Question").val()},
            handleSaveResult);
    });
}