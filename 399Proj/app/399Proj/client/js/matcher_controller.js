function handleMatchResult(resp_body) {
    console.log("handleMatchResult: resp_body: " + JSON.stringify(resp_body));
    $("#result").empty();
    $("#result").append('<div id="questionList"></div>');
    var len = resp_body.result.length - 1;
    for(var i in resp_body.result) {
        var head = "<b>Company:</b> " + resp_body.result[i].company  + " <b>Type:</b> " + resp_body.result[i].qtype + " <b>Sub Type:</b> " + resp_body.result[i].sub_type;
        var pre = "<pre>";
        var end = "</pre>";
        var line = "<hr>";
        var question = pre + resp_body.result[i].question + end;
        $("#questionList").append(head);
        $("#questionList").append(question);
        if ( i != len )
            $("#questionList").append(line);
    }
}

var index_main = function () {
    $("#Login").on("click", function (event) {
        window.location.replace("Login.html");
    });
    $("#Create").on("click", function (event) {
        window.location.replace("Create.html");
    });
    $("#Input").on("click", function (event) {
        window.location.replace("Input.html");
    });
    $("#match").on("click", function (event) {
        $.get("match.json",
            {"company": $("#Company").val(), "qtype": $("#QType").val(), "sub_type": $("#SubType").val()},
            handleMatchResult);
    });
}