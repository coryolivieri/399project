var express = require("express"),
    mongoose = require("mongoose"),
    http = require("http"),
    connect = require("connect"),  //npm install connect
    model = require("./model.js"),
    app = express(),
    port = process.env.PORT || 3000;

app.use(connect.urlencoded());  //this allows req.body

// set up a static file directory to use for default routing
app.use(express.static(__dirname + "/client"));

// Create our Express-powered HTTP server // and have it listen on port 3000

http.createServer(app).listen(port);
// set up our routes
app.get("/retrieve.json", function(req,res){
    var the_body = req.query;
    model.mongoGet(the_body, function (janswer){res.json(janswer);})
});

app.get("/passReset.json", function(req,res){
    var the_body = req.query;
    model.mongoPassReset(the_body, function (janswer){res.json(janswer);})
});

app.post("/save.json", function(req,res){
    var the_body = req.body;
    model.mongoSave(the_body, function (janswer){res.json(janswer);})
});

app.get("/passResetCheck.json", function(req,res){
    var the_body = req.query;
    model.mongoPassResetCheck(the_body, function (janswer){res.json(janswer);})
});

app.post("/newPass.json", function(req,res){
    var the_body = req.body;
    model.mongoNewPass(the_body, function (janswer){res.json(janswer);})
});

app.get("/match.json", function(req, res) {
    var the_body = req.query;
    model.mongoMatch(the_body, function (janswer) {res.json(janswer);})
});

app.post("/saveQuestion.json", function(req,res){
    var the_body = req.body;
    model.mongoSaveQuestion(the_body, function (janswer){res.json(janswer);})
});


