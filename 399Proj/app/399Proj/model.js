var mongoose = require("mongoose"),
    mongoUrl;

    // set up our services
if (process.env.VCAP_SERVICES) {
           services = JSON.parse(process.env.VCAP_SERVICES);
           console.log( JSON.stringify( services ));
           mongoUrl = services["mongolab"][0].credentials.uri;
} else {
           //use this when not running on Cloud Foundry
           mongoUrl = "mongodb://localhost/profiles";
}

//test out moongoose
mongoose.connect(mongoUrl);  //need createConnection because have two - see login.js

var db = mongoose.connection;

db.once('open', function () {
  console.log( "moongoose User info open!")});
db.on('error', console.error.bind(console, 'User info connection error:'));
db.once('connecting', function () {
  console.log( "user info connecting!")});
db.once('connected', function () {
  console.log( "user info connection!")});
db.on('disconnecting', function () {
  console.log( "user info disconnecting!")});
process.on('SIGINT', function() {
     db.close(function () {
       console.log('User info disconnected through app termination');
       process.exit(0);
  });
});

var profile_schema = mongoose.Schema({"username":String, "password":String, "SQAnswers":String,
"SQ":String});
var profile_model = mongoose.model("User Login Info", profile_schema);

var question_schema = mongoose.Schema({"company": String, "qtype": String, "sub_type": String, "question": String});

var question_model = mongoose.model("Questions", question_schema);


function mongoGet(body, callBack){
	profile_model.find({"username":body.username, "password":body.password}, function(err,result){
		if(result.length == 0) callBack({"message":"not found"});
		else {
			
			callBack({"message":"found"});
		};
	});
}

function mongoPassReset(body, callBack){
	profile_model.find({"username":body.username}, function(err,result){
		if(result.length == 0) callBack({"message":"not found"});
		else {
			callBack({"message":"found", "securityQ":result[0].SQ});
		};
	});
}


function mongoSave(body, callBack){ 
	profile_model.find({"username":body.username}, 
		function(err,doc){
			if(doc =="") { 
				profile_model.findOneAndUpdate({"username":body.username, "password":body.password, "SQAnswers":body.SQAnswers, "SQ":body.SQ}, 
					{}, {"upsert":true, "new":true}, function(err, doc){
					console.log("saved!");
					callBack({"outcome":doc,"error":err});
				});}
			else { callBack({"message":"GetNew"}); }
		}
	);
}


function mongoPassResetCheck(body, callBack){
	profile_model.find({"username":body.username, "SQAnswers":body.SQAnswers}, function(err,result){
		if(result.length == 0) callBack({"message":"not found"});
		else {
			callBack({"message":"found"});
		};
	});
}

function mongoNewPass(body, callBack){
	profile_model.findOneAndUpdate({"username":body.username},{"password":body.password}, function(err,result){
		if(result.length == 0) callBack({"message":"not found"});
		else {
			callBack({"message":"Success!"});
		};
	});
}

function mongoMatch(body, callBack) {
    console.log("mongoMatch body: " + JSON.stringify(body));
    if( body.company != "" && body.qtype != "" && body.sub_type != "") {
        question_model.find({"company": body.company, "qtype": body.qtype, "sub_type": body.sub_type}, function (err, result) {
            callBack({"result": result});
        });
    }
    else if (body.company != "" && body.qtype != "") {
        question_model.find({"company": body.company, "qtype": body.qtype}, function (err, result) {
            callBack({"result": result});
        });
    }
    else if (body.company != "" && body.sub_type != "") {
        question_model.find({"company": body.company, "sub_type": body.sub_type}, function (err, result) {
            callBack({"result": result});
        });
    }
    else if (body.qtype != "" && body.sub_type != "") {
        question_model.find({"qtype": body.qtype, "sub_type": body.sub_type}, function (err, result) {
            callBack({"result": result});
        });
    }
    else if (body.company != "") {
        question_model.find({"company": body.company}, function (err, result) {
            callBack({"result": result});
        });
    }
    else if (body.qtype != "") {
        question_model.find({"qtype": body.qtype}, function (err, result) {
            callBack({"result": result});
        });
    }
    else if (body.sub_type != "") {
        question_model.find({"sub_type": body.sub_type}, function (err, result) {
            callBack({"result": result});
        });
    }
    else {
        question_model.find({}, function (err, result) {
            callBack({"result": result});
        });
    }

}

function mongoSaveQuestion(body, callBack) {
    question_model.findOneAndUpdate({"company":body.company, "qtype":body.qtype, "sub_type":body.sub_type, "question":body.question},
        {}, {"upsert":true, "new":true}, function(err, doc){
            console.log("Question saved!");
            callBack({"outcome":doc,"error":err});
        });
}

module.exports = {
    "mongoGet": mongoGet,
    "mongoSave": mongoSave,
    "mongoPassReset":mongoPassReset,
    "mongoPassResetCheck":mongoPassResetCheck,
    "mongoNewPass":mongoNewPass,
    "mongoMatch":mongoMatch,
    "mongoSaveQuestion":mongoSaveQuestion

};















